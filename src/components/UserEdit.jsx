import React, { useState, useContext } from "react";
import { UsersContext } from "api/UsersContext";

import {
  Button,
  Box,
  Form,
  FormField,
  Select,
  TextInput,
  Avatar,
} from "grommet";

const UserEdit = ({ user }) => {
  const [data, setData] = useState(user);
  const { update } = useContext(UsersContext);
  const updateUser = () => {
    update(user.login.uuid, data);
  };

  return (
    <>
      <Box align="center">
        <Avatar size="xlarge" src={user.picture.large} />
      </Box>
      <Box pad="medium">
        <Form
          value={data.name}
          onChange={(nextValue) => {
            setData((data) => ({ ...data, name: nextValue }));
          }}
        >
          <Box direction="row">
            <FormField label="Title">
              <Select id="title" options={["Mr", "Mrs", "Ms"]} name="title" />
            </FormField>

            <FormField label="First name" htmlfor="firstname">
              <TextInput id="firstname" name="first" />
            </FormField>

            <FormField label="Last name">
              <TextInput id="lastname" name="last" />
            </FormField>
          </Box>
        </Form>

        <Form
          value={data}
          onChange={(nextValue) => {
            setData((data) => ({ ...data, nextValue }));
          }}
        >
          <FormField label="E-mail">
            <TextInput id="email" name="email" />
          </FormField>
          <FormField label="Phone">
            <TextInput id="cell" name="cell" />
          </FormField>
        </Form>

        <Box align="end">
          <Button onClick={updateUser} primary label="Save" />
        </Box>
      </Box>
    </>
  );
};

export default UserEdit;
