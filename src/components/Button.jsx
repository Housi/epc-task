import React from "react";
import styled from "styled-components";

const BaseButton = styled.a`
  color: black;
  display: inline-block;
  border-radius: 5px;
  padding: 0.5em 1em;
  cursor: pointer;
  :not(:last-of-type) {
    margin-right: 0.5em;
  }
`;

const PrimaryButton = styled(BaseButton)`
  color: white;

  background-color: var(--theme-color);
  &:hover {
    background-color: var(--theme-shade);
  }
`;

const SecondaryButton = styled(BaseButton)`
  border: 3px solid var(--theme-color);
  &:hover {
    border-color: var(--theme-shade);
  }
`;

const ShadowButton = styled(BaseButton)`
  border: 3px solid var(--theme-color);
  box-shadow: -1px 1px 20px 8px rgba(248, 227, 0, 0.8);
  &:hover {
    border-color: var(--theme-shade);
  }
`;

const Button = ({ type = "primary", children, onClick }) => {
  if (type === "secondary")
    return <SecondaryButton onClick={onClick}>{children}</SecondaryButton>;
  if (type === "shadow")
    return <ShadowButton onClick={onClick}>{children}</ShadowButton>;
  return <PrimaryButton onClick={onClick}>{children}</PrimaryButton>;
};

export default Button;
