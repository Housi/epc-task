import React, { useState } from "react";
import { Button, Layer, Box } from "grommet";
import { Close } from "grommet-icons";

const Modal = ({ label, children }) => {
  const [isOpen, setOpen] = useState(false);
  const closeDialog = () => setOpen(false);
  const openDialog = () => setOpen(true);

  const Modal = () => (
    <Layer onEsc={() => setOpen(false)} onClickOutside={() => setOpen(false)}>
      <Box pad="small">
        <Box>
          <Button icon={<Close />} onClick={closeDialog} />
        </Box>
        <Box>{children}</Box>
      </Box>
    </Layer>
  );

  return (
    <>
      <Button icon={label} onClick={openDialog} />
      {isOpen && <Modal />}
    </>
  );
};

export default Modal;
