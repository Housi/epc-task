import React, { useContext } from "react";
import { UsersContext } from "api/UsersContext";
import { Button, Box, Text } from "grommet";
import { Trash, Edit } from "grommet-icons";
import Modal from "components/Modal";
import DnDList from "components/DnDList";
import UserEdit from "components/UserEdit";

const UserList = () => {
  const { users, setUsers, remove } = useContext(UsersContext);

  const UserListItem = ({ data }) => {
    return (
      <Box direction="row" align="center">
        <Box fill="horizontal">
          {data.name.title} {data.name.first} {data.name.last}
        </Box>
        <Box fill="horizontal">{data.email}</Box>
        <Box fill="horizontal" direction="row" justify="end">
          <Modal label={<Edit size="small" />}>
            <UserEdit user={data} />
          </Modal>
          <Button
            icon={<Trash size="small" />}
            onClick={() => {
              remove(data);
            }}
          />
        </Box>
      </Box>
    );
  };

  return <DnDList template={UserListItem} list={users} setList={setUsers} />;
};

export default UserList;
