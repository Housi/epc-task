import React, { useState } from "react";
import styled from "styled-components";
import { Box } from "grommet";

const Draggable = styled(Box)`
  ${({ isDroparea }) =>
    isDroparea &&
    `
      color: white !important;
      position: relative;
        &:before {
          content: "Drop Here";
          color: #687bf7;
          width: 100%;
          height: 100%;
          border: 2px dashed #687bf7;
          position: absolute;
          display: flex;
          justify-content: center;
          align-items: center;
        }
  `}
`;

const initialDnDState = {
  draggedFrom: null,
  draggedTo: null,
  isDragging: false,
  originalOrder: [],
  updatedOrder: [],
};

const DnDList = ({ list, setList, template }) => {
  const [dragAndDrop, setDragAndDrop] = useState(initialDnDState);

  const onDragStart = (event) => {
    const initialPosition = Number(event.currentTarget.dataset.position);
    setDragAndDrop({
      ...dragAndDrop,
      draggedFrom: initialPosition,
      isDragging: true,
      originalOrder: list,
    });
  };

  const onDragOver = (event) => {
    event.preventDefault();
    const draggedTo = Number(event.currentTarget.dataset.position);

    if (draggedTo !== dragAndDrop.draggedTo) {
      let newList = dragAndDrop.originalOrder;
      const draggedFrom = dragAndDrop.draggedFrom;
      const itemDragged = newList[draggedFrom];
      const remainingItems = newList.filter(
        (item, index) => index !== draggedFrom
      );

      newList = [
        ...remainingItems.slice(0, draggedTo),
        itemDragged,
        ...remainingItems.slice(draggedTo),
      ];
      setDragAndDrop({
        ...dragAndDrop,
        updatedOrder: newList,
        draggedTo: draggedTo,
      });
    }
  };

  const onDrop = () => {
    setList(dragAndDrop.updatedOrder);
    setDragAndDrop({
      ...dragAndDrop,
      draggedFrom: null,
      draggedTo: null,
      isDragging: false,
    });
  };

  const onDragLeave = () => {
    setDragAndDrop({
      ...dragAndDrop,
      draggedTo: null,
    });
  };

  const isDraggedOver = (index) =>
    dragAndDrop && dragAndDrop.draggedTo === Number(index);

  return (
    <Box gap="none">
      {list.map((data, index) => (
        <Draggable
          border="all"
          pad="xxsmall"
          background="white"
          data-position={index}
          key={data.login.username}
          draggable
          onDragStart={onDragStart}
          onDragOver={onDragOver}
          onDragLeave={onDragLeave}
          onDrop={onDrop}
          isDroparea={isDraggedOver(index)}
        >
          {template({ data })}
        </Draggable>
      ))}
    </Box>
  );
};

export default DnDList;
