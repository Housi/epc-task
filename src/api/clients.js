import Axios from "axios";

const randomUserAPI = Axios.create({
  baseURL: "https://randomuser.me/api",
});

const userAPI = Axios.create({
  baseURL: "http://localhost:3006/api",
});

export { userAPI, randomUserAPI };
