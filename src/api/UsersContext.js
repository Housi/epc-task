import React, { useState, useEffect } from "react";
import { userAPI, randomUserAPI } from "api/clients";

const UsersContext = React.createContext([{}, () => {}]);

const UsersProvider = (props) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetch = async () => {
    setLoading(true);
    const { data } = await userAPI.get();
    if (data.users.length > 0) {
      setUsers(data.users);
    } else {
      const { data } = await randomUserAPI.get("/", {
        params: { results: 10 },
      });
      setUsers(data.results);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetch();
  }, []);

  const update = (uuid, changes) =>
    setUsers((items) =>
      items.map((item) => {
        if (item.login.uuid === uuid) return { ...item, ...changes };
        return item;
      })
    );

  const remove = (value) =>
    setUsers((items) => items.filter((item) => item !== value));

  useEffect(() => {
    if (!loading) {
      userAPI.patch("/", { users });
    }
  }, [users, loading]);

  if (loading) return <div className="App">loading...</div>;

  return (
    <UsersContext.Provider
      value={{ users, fetch, setUsers, remove, update, loading }}
    >
      {props.children}
    </UsersContext.Provider>
  );
};

export { UsersContext, UsersProvider };
