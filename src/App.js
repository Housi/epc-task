import React from "react";
import { UsersProvider } from "api/UsersContext";
import { Grommet, Main, Heading } from "grommet";
import UserList from "components/UserList";

const theme = {
  global: {
    font: {
      size: "18px",
    },
  },
};

function App() {
  return (
    <UsersProvider>
      <Grommet theme={theme} full={true}>
        <Main pad="large" background="light-3" fill="vertical">
          <Heading>User List</Heading>
          <UserList />
        </Main>
      </Grommet>
    </UsersProvider>
  );
}

export default App;
